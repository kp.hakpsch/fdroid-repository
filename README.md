# Official F-Droid repository for the luca app

[luca](https://luca-app.de) ensures a data protection-compliant, decentralized encryption of your data, undertakes the obligation to record contact data for events and gastronomy, relieves the health authorities through digital, lean, and integrated processes to enable efficient and complete tracing.

## How to use Luca Android App on F-Droid
If you can't or don't want to install the luca app via the Google Play Store, add this repository to [F-Droid](https://www.f-droid.org/) instead:
`https://lucaapp.gitlab.io/fdroid-repository/fdroid/repo`

[![Repo URL QRcode](fdroid/public/repo-qrcode.png)](https://lucaapp.gitlab.io/fdroid-repository/fdroid/repo)

For further instructions, please refer to the [english](https://f-droid.org/en/tutorials/add-repo/) or [german](https://f-droid.org/de/tutorials/add-repo/) userguide.

## Acknowledgements
This project is based on [rfc2822/fdroid-firefox](https://gitlab.com/rfc2822/fdroid-firefox).

## Issues & Support

Please [create an issue](https://gitlab.com/lucaapp/fdroid-repository/-/issues) for
suggestions or problems related to this repository. For general questions,
please check out our [FAQ](https://www.luca-app.de/faq/) or contact our support
team at [hello@luca-app.de](mailto:hello@luca-app.de).

## Licence
```
SPDX-License-Identifier: MIT

SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
```

For details see
* [licence file](https://gitlab.com/lucaapp/fdroid-repository/-/blob/master/LICENSE)
